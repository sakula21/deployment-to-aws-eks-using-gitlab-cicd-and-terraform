# eks-config

This project is used to manage the EKS cluster for Review Apps.
`review-apps` is the name of the cluster on AWS and related resources
(e.g., VPCs, etc.)

## Set AWS Credentials

Set the following AWS environment variables.

* `AWS_ACCESS_KEY_ID`
* `AWS_SECRET_ACCESS_KEY`
* `AWS_DEFAULT_REGION`

## Provision a new Kubernetes Cluster

```bash
bin/provision-cluster.bash
```

`AWS_ACCESS_KEY_ID` will be recorded to `aws-access-key-id-of-provisioner.txt`.
That's important because whoever provisioned the cluster is the only one who
can manage the cluster, including deprovisioning.

## Deporvision Kubernetes Cluster

Note: only the user who provisioned the cluster can deprovision it.

```bash
bin/deprovision-cluster.bash
```
