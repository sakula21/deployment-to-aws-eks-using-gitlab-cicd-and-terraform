set -xe

docker compose run --rm eksctl create cluster --name review-apps --managed --spot
echo AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID > aws-access-key-id-of-provisioner.txt
git add .
git commit -m "chore: provision cluster"
git push
