set -xe

docker compose run --rm eksctl delete cluster --name review-apps --wait
git add .
git commit -m "chore: deprovision cluster"
git push
